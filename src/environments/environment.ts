// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    // Initialize Firebase
    firebase:{
      apiKey: "AIzaSyDvVCv9dPeI8fX8XS-ersH8NGb3mAGxPlQ",
      authDomain: "fbreg-c2988.firebaseapp.com",
      databaseURL: "https://fbreg-c2988.firebaseio.com",
      projectId: "fbreg-c2988",
      storageBucket: "",
      messagingSenderId: "445052242848"
    }
   
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
